

center_of_trade = 1
culture = cave_goblin
religion = goblinic_shamanism
trade_goods = unknown
hre = no
base_tax = 2
base_production = 3
base_manpower = 3
native_size = 6
native_ferocity = 1
native_hostileness = 3